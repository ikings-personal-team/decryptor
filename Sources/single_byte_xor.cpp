//
//  single_byte_xor.cpp
//  Decryptor
//
//  Created by iKing on 21.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#include "decoders.hpp"
#include "helpers.hpp"
#include <cmath>

namespace decoders {
	
	std::vector<uint8_t> decodeSingleByteXor(const std::vector<uint8_t>& encoded, uint8_t key) {
		std::vector<uint8_t> decoded;
		for (int i = 0; i < encoded.size(); ++i) {
			decoded.push_back((uint8_t) encoded[i] ^ key);
		}
		return decoded;
	}
	
	std::vector<uint8_t> decodeSingleByteXor(const std::vector<uint8_t>& encoded) {
		auto charactersUsage = statistics::charactersUsage(statistics::alphabets::all);
		
		std::map<uint8_t, double> keysDelta;
		
		for (uint8_t key = 0;; ++key) {
			std::vector<uint8_t> decodedBytes = decodeSingleByteXor(encoded, key);
			std::string decoded = std::string(decodedBytes.begin(), decodedBytes.end());
			
			uint32_t totalCharactersCount = 0;
			std::map<char, uint32_t> charactersCount;
			for (auto entry: charactersUsage) {
				charactersCount[entry.first] = 0;
			}
			
			for (char character: decoded) {
				try {
					charactersCount.at(tolower(character))++;
					totalCharactersCount++;
				} catch (...) { }
			}
			
			if (totalCharactersCount != 0) {
				double delta = 0;
				for (auto entry: charactersCount) {
					double characterUsage = charactersUsage.at(entry.first);
					double difference = fabs((entry.second / (double) totalCharactersCount) - characterUsage);
					delta += difference;
				}
				keysDelta[key] = delta;
			}
			
			if (key == UINT8_MAX) {
				break;
			}
		}
		
		uint8_t key = std::min_element(keysDelta.begin(), keysDelta.end(),
									   [] (const std::pair<uint8_t, double>& lhs, const std::pair<uint8_t, double>& rhs) {
			return lhs.second < rhs.second;
		})->first;
		
		return decodeSingleByteXor(encoded, key);
	}
	
	std::string decodeSingleByteXor(const std::string& encoded) {
		if (encoded.empty()) {
			return std::string();
		}
		
		std::vector<uint8_t> decodedBytes = decodeSingleByteXor(helpers::hexToBytes(encoded));
		return std::string(decodedBytes.begin(), decodedBytes.end());
	}
	
	std::string decodeSingleByteXor(const std::string& encoded, uint8_t key) {
		std::vector<uint8_t> decoded = decodeSingleByteXor(std::vector<uint8_t>(encoded.begin(), encoded.end()), key);
		return std::string(decoded.begin(), decoded.end());
	}
} // decoders
