//
//  statistics.cpp
//  Decryptor
//
//  Created by iKing on 21.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#include "statistics.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>

namespace decoders {
namespace statistics {
	
	namespace alphabets {
		
		const std::map<char, double> monograms = [] () {
			std::map<char, double> bigrams;
			
			char bigram;
			double usage;
			std::ifstream stream("supporting/monograms.usage");
			while (stream >> bigram >> usage) {
				bigrams[bigram] = usage;
			}
			
			return bigrams;
		}();
		
		const std::map<std::string, double> bigrams = [] () {
			std::map<std::string, double> bigrams;
			
			std::string bigram;
			double usage;
			std::ifstream stream("supporting/bigrams.usage");
			while (stream >> bigram >> usage) {
				bigrams[bigram] = usage;
			}
			
			return bigrams;
		}();
		
		const std::map<std::string, double> trigrams = [] () {
			std::map<std::string, double> trigrams;
			
			std::string trigram;
			double usage;
			std::ifstream stream("supporting/trigrams.usage");
			while (stream >> trigram >> usage) {
				trigrams[trigram] = usage;
			}
			
			return trigrams;
		}();
		
		const std::map<std::string, double> quadgrams = [] () {
			std::map<std::string, double> quadgrams;
			
			std::string quadgram;
			double usage;
			std::ifstream stream("supporting/quadgrams.usage");
			while (stream >> quadgram >> usage) {
				quadgrams[quadgram] = usage;
			}
			
			return quadgrams;
		}();
		
		const std::map<std::string, double> quintgrams = [] () {
			std::map<std::string, double> quintdgrams;
			
			std::string quintdgram;
			double usage;
			std::ifstream stream("supporting/quintgrams.usage");
			while (stream >> quintdgram >> usage) {
				quintdgrams[quintdgram] = usage;
			}
			
			return quintdgrams;
		}();
		
		const std::map<std::string, double> words = [] () {
			std::map<std::string, double> quintdgrams;
			
			std::string quintdgram;
			double usage;
			std::ifstream stream("supporting/words.usage");
			while (stream >> quintdgram >> usage) {
				std::transform(quintdgram.begin(), quintdgram.end(), quintdgram.begin(), ::tolower);
				quintdgrams[quintdgram] = usage;
			}
			
			return quintdgrams;
		}();
		
		const std::vector<std::string> keywords = [] () {
			std::vector<std::string> keywords;
			
			std::string keyword;
			std::ifstream stream("supporting/keywords.usage");
			while (stream >> keyword) {
				std::transform(keyword.begin(), keyword.end(), keyword.begin(), ::tolower);
				keywords.push_back(keyword);
			}
			
			std::sort(keywords.begin(), keywords.end(), [] (const std::string& lhs, const std::string& rhs) {
				return lhs.length() > rhs.length();
			});
			
			return keywords;
		}();
		
	} // alphabets
	
	static const std::map<char, double> charactersPer1000Words = {
		{ 'a', 391.19930 },
		{ 'b',  71.46680 },
		{ 'c', 133.25780 },
		{ 'd', 203.71870 },
		{ 'e', 608.42580 },
		{ 'f', 106.72120 },
		{ 'g',  96.51850 },
		{ 'h', 291.90260 },
		{ 'i', 333.67140 },
		{ 'j',   7.32870 },
		{ 'k',  36.97880 },
		{ 'l', 192.79750 },
		{ 'm', 115.24740 },
		{ 'n', 323.27710 },
		{ 'o', 359.58530 },
		{ 'p',  92.39910 },
		{ 'q',   4.55050 },
		{ 'r', 286.77730 },
		{ 's', 303.06330 },
		{ 't', 433.78240 },
		{ 'u', 132.10820 },
		{ 'v',  46.84620 },
		{ 'w', 113.04400 },
		{ 'x',   7.18500 },
		{ 'y',  94.55460 },
		{ 'z',   3.54460 },
		
		{ '.',  65.30000 },
		{ ',',  61.60000 },
		{ ';',   3.20000 },
		{ ':',   3.40000 },
		{ '!',   3.30000 },
		{ '?',   5.60000 },
		{'\'',  24.30000 },
		{ '"',  26.70000 },
		{ '-',  15.30000 },
		{ ' ', 999.00000 }
	};
	
	std::map<char, double> charactersUsage(const std::string& alphabet, bool alphabetOnly) {
		double totalCharacters = std::accumulate(charactersPer1000Words.begin(), charactersPer1000Words.end(), 0.0,
												 [&alphabet, alphabetOnly] (double lhs, const std::pair<char, double>& rhs) {
			if (alphabetOnly) {
				return lhs + ((alphabet.find(rhs.first) != std::string::npos) ? rhs.second : 0.0);
			}
			return lhs + rhs.second;
		});
		
		std::map<char, double> usage;
		for (char character: alphabet) {
			try {
				char ch = tolower(character);
				double count = charactersPer1000Words.at(ch);
				usage[ch] = count / totalCharacters;
			} catch (...) { }
		}
		
		return usage;
	}
	
} // statistics
} // decoders
