//
//  polyalphabetic_substitution.cpp
//  Decryptor
//
//  Created by iKing on 21.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#include "decoders.hpp"
#include "helpers.hpp"
#include <cmath>
#include <algorithm>
#include <limits>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <mutex>
#include <set>
#include <sstream>

namespace decoders {
	
	static const size_t MIN_MUTATIONS_COUNT = 1;
	static const size_t WORKERS_COUNT = 8;
	static const size_t ITERATIONS_COUNT = 500000;
	static const size_t POPULATION_SIZE = 1000;
	
	struct Item {
		std::vector<std::string> keys;
		double delta;
	};
	
	std::string decode(std::vector<std::string> encodedComponents, std::vector<std::map<char, char> > transitions) {
		std::vector<std::string> decodedComponents;
		for (size_t i = 0; i < encodedComponents.size(); ++i) {
			decodedComponents.push_back(helpers::simple_substitution::decode(encodedComponents[i], transitions[i]));
		}
		
		std::string decoded;
		
		while (std::count_if(decodedComponents.begin(), decodedComponents.end(),
							 [] (const std::string& component) { return !component.empty(); } )) {
			for (uint8_t i = 0; i < decodedComponents.size(); ++i) {
				std::string& component = decodedComponents[i];
				
				if (component.empty()) {
					continue;
				}
				
				decoded += *component.begin();
				component.erase(component.begin());
			}
		}
		
		return decoded;
	}
	
	std::string decode(const std::vector<std::string>& encodedComponents, const std::vector<std::string>& keys, const std::string& alphabet) {
		std::vector<std::map<char, char> > transitions;
		for (size_t i = 0; i < encodedComponents.size(); ++i) {
			std::string key = keys[i];
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			std::map<char, char> currentTransitions;
			
			for (size_t j = 0; j < key.length(); ++j) {
				currentTransitions[key[j]] = alphabet[j];
			}
			
			transitions.push_back(currentTransitions);
		}
		
		return decode(encodedComponents, transitions);
	}
	
	Item mutateItem(const Item& item, std::function<double(const std::vector<std::string>&)> deltaCalculator) {
		std::vector<std::string> currentKeys = item.keys;
		size_t mutationsCount = MIN_MUTATIONS_COUNT + rand() % (currentKeys.size() * 2);
		for (size_t j = 0; j < mutationsCount; ++j) {
			size_t keyIndex = rand() % currentKeys.size();
			std::string& key = currentKeys[keyIndex];
			int firstIndex = rand() % key.length();
			while (isupper(key[firstIndex])) {
				firstIndex = rand() % key.length();
			}
			int secondIndex = rand() % key.length();
			while (isupper(key[secondIndex])) {
				secondIndex = rand() % key.length();
			}
			std::swap(key[firstIndex], key[secondIndex]);
		}
		
		Item mutated = Item { currentKeys, deltaCalculator(currentKeys) };
		
		return mutated;
	}
	
	std::vector<Item> mutateItem(const Item& item, size_t mutationsCount, std::function<double(const std::vector<std::string>&)> deltaCalculator) {
		std::vector<Item> mutations(mutationsCount);
		for (size_t i = 0; i < mutationsCount; ++i) {
			mutations[i] = mutateItem(item, deltaCalculator);;
		}
		return mutations;
	}
	
	std::string decodePoylayphabeticSubstitution(const std::string& encoded, std::function<double(const std::string&)> deltaCalculator) {
		
		std::vector<std::pair<uint8_t, double> > iocs;
		for (uint8_t step = 1; step < 64; ++step) {
			size_t match = 0;
			size_t total = 0;
			for (size_t i = 0; i < encoded.size(); ++i) {
				for (size_t j = i + step; j < encoded.size(); j += step) {
					total++;
					if (encoded[i] == encoded[j]) {
						match++;
					}
				}
			}
			iocs.push_back(std::make_pair(step, (double) match / total));
		}
		
		auto maxIoc = *std::max_element(iocs.begin(), iocs.end(),
										[] (const std::pair<uint8_t, double>& lhs, const std::pair<uint8_t, double>& rhs) {
											return lhs.second < rhs.second;
										});
		
		const double eps = maxIoc.second * 0.15;
		std::vector<std::pair<uint8_t, double> > possibleIocs;
		std::copy_if(iocs.begin(), iocs.end(), std::back_inserter(possibleIocs), [&maxIoc, &eps] (const std::pair<uint8_t, double>& ioc) {
			return ioc.second >= maxIoc.second - eps;
		});
		
		std::map<uint8_t, uint8_t> possibleGcds;
		for (size_t i = 0; i < possibleIocs.size(); ++i) {
			if (i == 0) {
				possibleGcds[possibleIocs[i].first]++;
			} else {
				possibleGcds[possibleIocs[i].first - possibleIocs[i - 1].first]++;
			}
		}
		uint8_t gcd = std::max_element(possibleGcds.begin(), possibleGcds.end(),
									   [] (const std::pair<uint8_t, uint8_t>& lhs, const std::pair<uint8_t, uint8_t>& rhs) {
			return lhs.second < rhs.second;
		})->first;
		
		auto ioc = std::find_if(possibleIocs.begin(), possibleIocs.end(), [gcd] (const std::pair<uint8_t, double>& ioc) {
			return ioc.first % gcd == 0;
		});
		
		if (ioc == possibleIocs.end()) {
			return std::string();
		}
		
		uint8_t keyLength = ioc->first;
		std::vector<std::string> encodedComponents(keyLength);
		for (size_t i = 0; i < encoded.length(); ++i) {
			encodedComponents[i % keyLength].push_back(encoded[i]);
		}
		
		auto expectedMonogramsUsageMap = statistics::alphabets::monograms;
		std::vector<std::pair<char, double> > expectedMonogramsUsage(expectedMonogramsUsageMap.begin(), expectedMonogramsUsageMap.end());
		std::sort(expectedMonogramsUsage.begin(), expectedMonogramsUsage.end(), [] (const std::pair<char, double>& lhs, const std::pair<char, double>& rhs) {
			return lhs.second > rhs.second;
		});
		
		std::string alphabet;
		for (auto character: expectedMonogramsUsage) {
			alphabet += character.first;
		}
		
		std::vector<std::string> keys;
		for (auto component: encodedComponents) {
			std::vector<std::pair<char, double> > charactersUsage = helpers::calculateCharactersUsage(component);
			
			if (charactersUsage.size() > alphabet.length()) {
				// TODO: Something gone wrong...
				return std::string();
			}
			
			std::string key;
			for (auto character: charactersUsage) {
				key += character.first;
			}
						
			std::copy_if(alphabet.begin(), alphabet.end(), std::back_inserter(key), [&key] (char ch) {
				return key.find(ch) == std::string::npos;
			});
			
			keys.push_back(key);
		}
		
		auto calculator = [] (const std::string& str) {
			std::string string = str;
			string.erase(std::remove(string.begin(), string.end(), ' '), string.end());
			
			double delta = 0.0;
			
			static const std::vector<std::pair<std::string, double> > specificWords = {
				{ "decipher", 1.0 },
				{ "encrypt", 1.0 },
				{ "paragraph", 1.0 }
			};
			
			for (const auto& word: specificWords) {
				size_t pos = string.find(word.first);
				while (pos != std::string::npos) {
					delta -= 0.001 * word.first.length() * word.second;
					pos = string.find(word.first, pos + word.first.length());
				}
			}
			
			delta += helpers::calculateQuadgramsDelta(string);
			
			return delta;
		};
		
		const size_t populationSize = POPULATION_SIZE;
		
		std::vector<Item> population = {
			Item { keys, calculator(decode(encodedComponents, keys, alphabet)) }
		};
		
		std::vector<Item> mutated = mutateItem(population[0], populationSize - 1, [&] (std::vector<std::string> keys) {
			return calculator(decode(encodedComponents, keys, alphabet));
		});
		
		population.insert(population.end(), mutated.begin(), mutated.end());
		
		const size_t countToStayAlive = population.size() / (keys.size() + 1);
		
		for (size_t i = 0; i < ITERATIONS_COUNT; ++i) {
			std::sort(population.begin(), population.end(), [] (const Item& lhs, const Item& rhs) {
				return lhs.delta < rhs.delta;
			});
			std::vector<Item> generation(population.begin(), population.begin() + countToStayAlive);
			population.erase(population.begin() + countToStayAlive, population.end());
			
			
			static const size_t workersCount = WORKERS_COUNT;
			std::mutex mutex;
			std::vector<std::thread> workers;
			size_t itemsPerWorker = generation.size() / workersCount;
			
			auto workerTask = [&mutex, &calculator, &encodedComponents, &alphabet] (const size_t startIndex, const size_t endIndex, const std::vector<Item>& items, std::vector<Item>& destination) {
				std::vector<Item> newPopulation;
				
				for (size_t i = startIndex; i < endIndex; ++i) {
					Item item = items[i];
					
					if (item.keys.size() == 0) {
						std::cout << item.keys.size() << std::endl;
						std::cout << destination.size() << std::endl;
					}
					
					std::vector<Item> mutations = mutateItem(item, item.keys.size(), [=] (const std::vector<std::string>& keys) {
						return calculator(decode(encodedComponents, keys, alphabet));
					});
					
					newPopulation.insert(newPopulation.end(), mutations.begin(), mutations.end());
				}
				
				mutex.lock();
				for (const Item& item: newPopulation) {
					auto it = std::find_if(destination.begin(), destination.end(), [&item] (const Item& entry) {
						return entry.keys == item.keys;
					});
					if (it == destination.end()) {
						destination.push_back(item);
					}
				}
				mutex.unlock();
			};
			
			for (size_t j = 0; j < workersCount; ++j) {
				size_t startIndex = itemsPerWorker * j;
				size_t endIndex = j == workersCount - 1 ? generation.size() : itemsPerWorker * (j + 1);
				workers.push_back(std::thread(workerTask, startIndex, endIndex, std::ref(generation), std::ref(population)));
			}
			
			for (std::thread& worker: workers) {
				worker.join();
			}
			
			if (i % 10 == 0) {
				std::cout << i << std::endl;
				Item item = *std::min_element(population.begin(), population.end(), [] (const Item& lhs, const Item& rhs) {
					return lhs.delta < rhs.delta;
				});
				std::cout << "Delta: " << item.delta << std::endl;
				std::cout << alphabet << " – alphabet" << std::endl;
				std::vector<std::string> bestKeys = item.keys;
				
				for (std::string& key: bestKeys) {
					std::cout << key << std::endl;
				}

				std::cout << decode(encodedComponents, bestKeys, alphabet) << std::endl;
			}
		}
		
		std::vector<std::string> bestKeys = std::min_element(population.begin(), population.end(), [] (const Item& lhs, const Item& rhs) {
			return lhs.delta < rhs.delta;
		})->keys;
		
		std::string decoded = decode(encodedComponents, bestKeys, alphabet);
		
		return decoded;
	}
	
} // decoders
