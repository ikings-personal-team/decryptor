//
//  helpers.cpp
//  Decryptor
//
//  Created by iKing on 18.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#include "helpers.hpp"
#include <stdlib.h>
#include <cmath>
#include <thread>
#include <mutex>
#include <iostream>
#include "statistics.hpp"

namespace decoders {
namespace helpers {
	
	std::vector<uint8_t> hexToBytes(const std::string& hex) {
		std::vector<uint8_t> bytes;
		
		for (unsigned int i = 0; i < hex.length(); i += 2) {
			std::string byteString = hex.substr(i, 2);
			char byte = (uint8_t) strtol(byteString.c_str(), NULL, 16);
			bytes.push_back(byte);
		}
		
		return bytes;
	}
	
	std::vector<std::pair<char, double> > calculateCharactersUsage(const std::string& string) {
		std::map<char, size_t> charactersCount;
		for (auto character: string) {
			charactersCount[character]++;
		}
		
		std::vector<std::pair<char, double> > charactersUsage;
		for (auto entry: charactersCount) {
			charactersUsage.push_back(std::make_pair(entry.first, (double) entry.second / string.size()));
		}
		
		std::sort(charactersUsage.begin(), charactersUsage.end(),
				  [] (const std::pair<char, double>& lhs, const std::pair<char, double>& rhs) {
					  return lhs.second > rhs.second;
				  });
		
		return charactersUsage;
	}
	
	double calculateCharactersDelta(const std::string& string, const std::map<char, double>& expectedCharactersUsage) {
		auto charactersUsage = calculateCharactersUsage(string);
		
		double delta = 0;
		for (auto entry: charactersUsage) {
			double characterUsage = expectedCharactersUsage.at(entry.first);
			double difference = fabs(entry.second - characterUsage);
			delta += difference;
		}
		
		return delta;
	}
	
	double calculateBigramsDelta(const std::string& string) {
		size_t totalBigrams = string.length() - 1;
		std::map<std::string, uint32_t> bigrams;
		for (size_t i = 0; i < totalBigrams; ++i) {
			bigrams[string.substr(i, 2)]++;
		}
		
		std::vector<std::pair<std::string, double> > bigramsUsage;
		for (auto bigram: bigrams) {
			bigramsUsage.push_back(std::make_pair(bigram.first, (double) bigram.second / totalBigrams));
		}
		std::sort(bigramsUsage.begin(), bigramsUsage.end(),
				  [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
					  return lhs.second > rhs.second;
				  });
		
		double delta = 0;
		
		for (auto bigram: bigramsUsage) {
			double standardUsage = statistics::alphabets::bigrams.at(bigram.first);
			double difference = fabs(bigram.second - standardUsage);
			delta += difference;
		}
		
		return delta;
	}
	
	double calculateTrigramsDelta(const std::string& string, bool withIntersections) {
		size_t totalTrigrams = 0;
		std::map<std::string, uint32_t> trigrams;
		if (withIntersections) {
			totalTrigrams = string.length() - 2;
			for (size_t i = 0; i < totalTrigrams; ++i) {
				trigrams[string.substr(i, 3)]++;
			}
		} else {
			for (size_t i = 0; i < string.length() - 2; i += 3) {
				totalTrigrams++;
				trigrams[string.substr(i, 3)]++;
			}
		}
		
		std::vector<std::pair<std::string, double> > trigramsUsage;
		for (auto trigram: trigrams) {
			trigramsUsage.push_back(std::make_pair(trigram.first, (double) trigram.second / totalTrigrams));
		}
		std::sort(trigramsUsage.begin(), trigramsUsage.end(),
				  [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
					  return lhs.second > rhs.second;
				  });
		
		double delta = 0;
		
		for (auto trigram: trigramsUsage) {
			double standardUsage = 0.0;
			try {
				standardUsage = statistics::alphabets::trigrams.at(trigram.first);
			} catch (...) { }
			double difference = fabs(trigram.second - standardUsage);
			delta += difference;
		}
		
		return delta;
	}
	
	double calculateQuadgramsDelta(const std::string& string, bool withIntersections) {
		size_t totalQuadgrams = 0;
		std::map<std::string, uint32_t> quadgrams;
		if (withIntersections) {
			totalQuadgrams = string.length() - 3;
			for (size_t i = 0; i < totalQuadgrams; ++i) {
				quadgrams[string.substr(i, 4)]++;
			}
		} else {
			for (size_t i = 0; i < string.length() - 3; i += 4) {
				totalQuadgrams++;
				quadgrams[string.substr(i, 4)]++;
			}
		}
		
		std::vector<std::pair<std::string, double> > quadgramsUsage;
		for (auto quadgram: quadgrams) {
			quadgramsUsage.push_back(std::make_pair(quadgram.first, (double) quadgram.second / totalQuadgrams));
		}
		std::sort(quadgramsUsage.begin(), quadgramsUsage.end(),
				  [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
					  return lhs.second > rhs.second;
				  });
		
		double delta = 0;
		
		for (auto quadgram: quadgramsUsage) {
			double standardUsage = 0.0;
			try {
				standardUsage = statistics::alphabets::quadgrams.at(quadgram.first);
			} catch (...) { }
			double difference = fabs(quadgram.second - standardUsage);
			delta += difference;
		}
		
		return delta;
	}
	
	double calculateQuadgramsUsage(const std::string& string, bool withIntersections) {
		size_t totalQuadgrams = 0;
		std::map<std::string, uint32_t> quadgrams;
		if (withIntersections) {
			totalQuadgrams = string.length() - 4;
			for (size_t i = 0; i < totalQuadgrams; ++i) {
				quadgrams[string.substr(i, 4)]++;
			}
		} else {
			for (size_t i = 0; i < string.length() - 4; i += 4) {
				totalQuadgrams++;
				quadgrams[string.substr(i, 4)]++;
			}
		}
		
		std::vector<std::pair<std::string, double> > quadgramsUsage;
		for (auto quadgram: quadgrams) {
			quadgramsUsage.push_back(std::make_pair(quadgram.first, (double) quadgram.second / totalQuadgrams));
		}
		std::sort(quadgramsUsage.begin(), quadgramsUsage.end(),
				  [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
					  return lhs.second > rhs.second;
				  });
		
		double delta = 0;
		
		for (auto quadgram: quadgramsUsage) {
			double standardUsage = 0.0;
			try {
				standardUsage = statistics::alphabets::quadgrams.at(quadgram.first);
			} catch (...) { }
			if (standardUsage < 1e-9) {
				continue;
			}
			delta += quadgram.second;
		}
		
		return delta;
	}
	
	double getStandardTrigramUsage(const std::string& pattern) {
		static std::map<std::string, double> cache;
		
		if (pattern.length() != 3) {
			return 0.0;
		}
		
		try {
			return cache.at(pattern);
		} catch (...) { }
		
		std::vector<size_t> indices;
		for (size_t i = 0; i < pattern.length(); ++i) {
			if (pattern[i] != '*') {
				indices.push_back(i);
			}
		}
		
		size_t totalOccurrences = 0;
		double totalUsage = 0.0;
		
		for (auto trigram: statistics::alphabets::trigrams) {
			bool matchPattern = true;
			for (size_t index: indices) {
				if (trigram.first[index] != pattern[index]) {
					matchPattern = false;
					break;
				}
			}
			
			if (matchPattern) {
				totalOccurrences++;
				totalUsage += trigram.second;
			}
		}
		
		double usage = totalUsage;
		cache[pattern] = usage;
		
		return usage;
	}
	
	double calculateQuintgramsDelta(const std::string& string, bool withIntersections) {
		size_t totalQuintgrams = 0;
		std::map<std::string, uint32_t> quintgrams;
		if (withIntersections) {
			totalQuintgrams = string.length() - 4;
			for (size_t i = 0; i < totalQuintgrams; ++i) {
				quintgrams[string.substr(i, 5)]++;
			}
		} else {
			for (size_t i = 0; i < string.length() - 2; i += 3) {
				totalQuintgrams++;
				quintgrams[string.substr(i, 5)]++;
			}
		}
		
		std::vector<std::pair<std::string, double> > quintgramsUsage;
		for (auto quintgram: quintgrams) {
			quintgramsUsage.push_back(std::make_pair(quintgram.first, (double) quintgram.second / totalQuintgrams));
		}
		std::sort(quintgramsUsage.begin(), quintgramsUsage.end(),
				  [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
					  return lhs.second > rhs.second;
				  });
		
		double delta = 0;
		
		for (auto quintgram: quintgramsUsage) {
			double standardUsage = 0.0;
			try {
				standardUsage = statistics::alphabets::quintgrams.at(quintgram.first);
			} catch (...) { }
			double difference = fabs(quintgram.second - standardUsage);
			delta += difference;
		}
		
		return delta;
	}
	
	size_t calculateKeywordsCount(const std::string& string) {
		std::string copy = string;
		size_t count = 0;
		for (std::string keyword: statistics::alphabets::keywords) {
			size_t position = copy.find(keyword);
			while (position != std::string::npos) {
				count++;
				position = copy.find(keyword, position + keyword.size());
			}
		}
		
		return count;
	}
	
	std::pair<double, size_t> calculateKeywordsUsage(const std::string& string) {
		static const size_t workersCount = 4;

		std::mutex mutex;
		double usage = 0.0;
		size_t count = 0;
		size_t maxKeywordLength = statistics::alphabets::keywords.front().size();

		const std::vector<std::string>& keywords = statistics::alphabets::keywords;
		size_t keywordsCount = keywords.size();
		std::vector<std::thread> workers;
		size_t keywordsPerWorker = keywordsCount / workersCount;

		auto workerTask = [&usage, &count, &mutex, &maxKeywordLength, &string] (const std::vector<std::string> keywords) {
			double currentUsage = 0.0;
			size_t currentCount = 0;
			for (std::string keyword: keywords) {
				size_t position = string.find(keyword);
				while (position != std::string::npos) {
					currentUsage += (double) keyword.size() / maxKeywordLength;
					currentCount++;
					position = string.find(keyword, position + keyword.size());
				}
			}
			mutex.lock();
			usage += currentUsage;
			count += currentCount;
			mutex.unlock();
		};

		for (size_t i = 0; i < workersCount; ++i) {
			std::vector<std::string> currentKeywords(keywords.begin() + keywordsPerWorker * i, keywords.begin() + (i == workersCount - 1 ? keywordsCount : keywordsPerWorker * (i + 1)));
			workers.push_back(std::thread(workerTask, currentKeywords));
		}

		for (std::thread& worker: workers) {
			worker.join();
		}
		
		return std::make_pair(usage, count);
	}
	
	double calculateWordsDelta(const std::string& string) {
		double totalWords = (double) string.length() / 4.5;
		std::map<std::string, uint32_t> words;
		for (const auto& word: statistics::alphabets::words) {
			auto pos = string.find(word.first);
			while (pos != std::string::npos) {
				words[word.first]++;
//				totalWords++;
				pos = string.find(word.first, pos + word.first.length());
			}
		}
		
		double delta = 0;
		
		for (auto word: words) {
			double standardUsage = statistics::alphabets::words.at(word.first);
			double difference = fabs((double) (word.second / totalWords) - standardUsage);
			delta += difference;
		}
		
		return delta;
	}
	
	
	
	namespace simple_substitution {
		
		std::string decode(const std::string& encoded, const std::map<char, char>& transitions) {
			std::string decoded;
			for (auto character: encoded) {
				decoded += transitions.at(tolower(character));
			}
			
			return decoded;
		}
		
	} // simple_substitution
	
} // helpers
} // decoders
