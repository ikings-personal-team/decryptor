//
//  repeating_key_xor.cpp
//  Decryptor
//
//  Created by iKing on 21.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//


#include "decoders.hpp"
#include "helpers.hpp"

namespace decoders {
	
	std::string decodeRepeatingKeyXor(const std::string& encoded) {
		std::vector<uint8_t> encodedBytes = helpers::hexToBytes(encoded);
		
		std::vector<std::pair<uint8_t, double> > iocs;
		for (uint8_t step = 1; step < 64; ++step) {
			size_t match = 0;
			size_t total = 0;
			for (size_t i = 0; i < encodedBytes.size(); ++i) {
				for (size_t j = i + step; j < encodedBytes.size(); j += step) {
					total++;
					if (encodedBytes[i] == encodedBytes[j]) {
						match++;
					}
				}
			}
			iocs.push_back(std::make_pair(step, (double) match / total));
		}
		
		auto maxIoc = *std::max_element(iocs.begin(), iocs.end(),
										[] (const std::pair<uint8_t, double>& lhs, const std::pair<uint8_t, double>& rhs) {
			return lhs.second < rhs.second;
		});
		
		static const double eps = 0.05;
		std::vector<std::pair<uint8_t, double> > possibleIocs;
		std::copy_if(iocs.begin(), iocs.end(), std::back_inserter(possibleIocs), [maxIoc] (const std::pair<uint8_t, double>& ioc) {
			return ioc.second >= maxIoc.second - eps;
		});
		
		std::map<uint8_t, uint8_t> possibleGcds;
		for (size_t i = 0; i < possibleIocs.size(); ++i) {
			if (i == 0) {
				possibleGcds[possibleIocs[i].first]++;
			} else {
				possibleGcds[possibleIocs[i].first - possibleIocs[i - 1].first]++;
			}
		}
		uint8_t gcd = std::max_element(possibleGcds.begin(), possibleGcds.end(),
									   [] (const std::pair<uint8_t, uint8_t>& lhs, const std::pair<uint8_t, uint8_t>& rhs) {
			return lhs.second < rhs.second;
		})->first;
		
		auto ioc = std::find_if(possibleIocs.begin(), possibleIocs.end(), [gcd] (const std::pair<uint8_t, double>& ioc) {
			return ioc.first % gcd == 0;
		});
		
		if (ioc == possibleIocs.end()) {
			return std::string();
		}
		
		uint8_t keyLength = ioc->first;
		std::vector<std::vector<uint8_t> > encodedComponents(keyLength);
		for (size_t i = 0; i < encodedBytes.size(); ++i) {
			encodedComponents[i % keyLength].push_back(encodedBytes[i]);
		}
		
		std::vector<std::vector<uint8_t> > decodedComponents;
		for (auto component: encodedComponents) {
			decodedComponents.push_back(decodeSingleByteXor(component));
		}
		
		std::string decoded;
		while (std::count_if(decodedComponents.begin(), decodedComponents.end(),
							 [] (const std::vector<uint8_t>& component) { return !component.empty(); } )) {
			for (uint8_t i = 0; i < keyLength; ++i) {
				std::vector<uint8_t>& component = decodedComponents[i];
				
				if (component.empty()) {
					continue;
				}
				
				decoded += *component.begin();
				component.erase(component.begin());
			}
		}
		
		return decoded;
	}
} // decoders
