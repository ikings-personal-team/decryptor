//
//  decoders.hpp
//  Decryptor
//
//  Created by iKing on 18.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#ifndef decoders_hpp
#define decoders_hpp

#include "statistics.hpp"
#include "helpers.hpp"
#include <vector>
#include <string>
#include <functional>

namespace decoders {
	
	std::vector<uint8_t> decodeSingleByteXor(const std::vector<uint8_t>& encoded, uint8_t key);
	std::vector<uint8_t> decodeSingleByteXor(const std::vector<uint8_t>& encoded);
	std::string decodeSingleByteXor(const std::string& encoded);
	std::string decodeSingleByteXor(const std::string& encoded, uint8_t key);
	
	std::string decodeRepeatingKeyXor(const std::string& encoded);
		
	std::string decodeSimpleSubstitution(const std::string& encoded, std::function<double(const std::string&)> deltaCalculator = [] (const std::string& string) {
		return helpers::calculateTrigramsDelta(string);
	});
	
	std::string decodePoylayphabeticSubstitution(const std::string& encoded,
												 std::function<double(const std::string&)> deltaCalculator = [] (const std::string& string) {
													 return helpers::calculateTrigramsDelta(string);
												 });
	
} // decoders

#endif /* decoders_hpp */
