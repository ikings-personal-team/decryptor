//
//  helpers.hpp
//  Decryptor
//
//  Created by iKing on 18.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#ifndef helpers_hpp
#define helpers_hpp

#include <string>
#include <vector>
#include <map>

namespace decoders {
namespace helpers {

	std::vector<uint8_t> hexToBytes(const std::string& hex);
	template<typename T>
	void combine(const std::vector<std::vector<T> >& rows, std::vector<std::vector<T> >& combinations,
				 size_t index = 0, std::vector<T> current = std::vector<T>()) {
		if (index >= rows.size()) {
			combinations.push_back(current);
			return;
		}
		for (size_t i = 0; i < rows[index].size(); ++i) {
			std::vector<T> curr = std::vector<T>(current);
			curr.push_back(rows[index][i]);
			combine(rows, combinations, index + 1, curr);
		}
	}
	
	std::vector<std::pair<char, double> > calculateCharactersUsage(const std::string& string);
	double calculateCharactersDelta(const std::string& string, const std::map<char, double>& expectedCharactersUsage);
	double calculateBigramsDelta(const std::string& string);
	double calculateTrigramsDelta(const std::string& string, bool withIntersections = true);
	double calculateQuadgramsDelta(const std::string& string, bool withIntersections = true);
	double calculateQuadgramsUsage(const std::string& string, bool withIntersections = true);
	double calculateQuintgramsDelta(const std::string& string, bool withIntersections = true);
	
	double getStandardTrigramUsage(const std::string& trigramPattern);
	
	size_t calculateKeywordsCount(const std::string& string);
	std::pair<double, size_t> calculateKeywordsUsage(const std::string& string);
	
	double calculateWordsDelta(const std::string& string);
	
	namespace simple_substitution {
		
		std::string decode(const std::string& encoded, const std::map<char, char>& transitions);
		
	} // simple_substitution
	
} // helpers
} // decoders

#endif /* helpers_hpp */
