//
//  simple_substitution.cpp
//  Decryptor
//
//  Created by iKing on 21.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#include "decoders.hpp"
#include "helpers.hpp"
#include <cmath>
#include <algorithm>
#include <limits>
#include <iostream>

namespace decoders {
	
	std::map<char, char> makeTransitions(const std::string& key, const std::string& alphabet) {
		std::map<char, char> transitions;
		for (size_t i = 0; i < key.length(); ++i) {
			transitions[key[i]] = alphabet[i];
		}
		return transitions;
	}

	std::string decodeSimpleSubstitution(const std::string& encoded, std::function<double(const std::string&)> deltaCalculator) {
		using namespace helpers::simple_substitution;
		
		auto expectedMonogramsUsageMap = statistics::alphabets::monograms;
		std::vector<std::pair<char, double> > expectedMonogramsUsage(expectedMonogramsUsageMap.begin(), expectedMonogramsUsageMap.end());
		std::sort(expectedMonogramsUsage.begin(), expectedMonogramsUsage.end(), [] (const std::pair<char, double>& lhs, const std::pair<char, double>& rhs) {
			return lhs.second > rhs.second;
		});
		
		std::vector<std::pair<char, double> > encodedCharactersUsage = helpers::calculateCharactersUsage(encoded);
		
		if (encodedCharactersUsage.size() > expectedMonogramsUsage.size()) {
			return std::string();
		}
		
		std::string alphabet;
		for (auto character: expectedMonogramsUsage) {
			alphabet += character.first;
		}
		
		std::string key;
		for (auto character: encodedCharactersUsage) {
			key += character.first;
		}
		
		
		std::vector<std::pair<std::string, double> > population = {
			std::make_pair(key, helpers::calculateTrigramsDelta(decode(encoded, makeTransitions(key, alphabet))))
		};
		
		for (size_t i = 0; i < 49; ++i) {
			std::string currentKey = key;
			int firstIndex = rand() % currentKey.length();
			int secondIndex = rand() % currentKey.length();
			std::swap(currentKey[firstIndex], currentKey[secondIndex]);
			population.push_back(std::make_pair(currentKey, helpers::calculateTrigramsDelta(decode(encoded, makeTransitions(currentKey, alphabet)))));
		}
		
		for (size_t i = 0; i < 200; ++i) {
			std::sort(population.begin(), population.end(), [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
				return lhs.second < rhs.second;
			});
			std::vector<std::pair<std::string, double> > generation;
			for (size_t j = 0; j < population.size() / 2; ++j) {
				int firstIndex = rand() % population.size() / 2;
				int secondIndex = rand() % population.size() / 2;
				if (population[firstIndex].second < population[secondIndex].second) {
					generation.push_back(population[firstIndex]);
				} else {
					generation.push_back(population[secondIndex]);
				}
			}
			
			size_t personsToAdd = population.size() - generation.size();
			for (size_t j = 0; j < personsToAdd; ++j) {
				int index = rand() % generation.size();
				std::string currentKey = generation[index].first;
				int firstIndex = rand() % currentKey.length();
				int secondIndex = rand() % currentKey.length();
				std::swap(currentKey[firstIndex], currentKey[secondIndex]);
				generation.push_back(std::make_pair(currentKey, helpers::calculateTrigramsDelta(decode(encoded, makeTransitions(currentKey, alphabet)))));
			}
			
			population = generation;
			
			if (i % 100 == 0) {
				std::cout << i << std::endl;
			}
		}
		
		std::string bestKey = std::min_element(population.begin(), population.end(), [] (const std::pair<std::string, double>& lhs, const std::pair<std::string, double>& rhs) {
			return lhs.second < rhs.second;
		})->first;
		
		std::string decoded = decode(encoded, makeTransitions(bestKey, alphabet));
		
		return decoded;
	}
	
} // decoders
