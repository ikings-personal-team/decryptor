//
//  statistics.hpp
//  Decryptor
//
//  Created by iKing on 21.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#ifndef statistics_hpp
#define statistics_hpp

#include <map>
#include <vector>
#include <algorithm>
#include <fstream>

namespace decoders {
namespace statistics {
	
	namespace alphabets {
		
		static const std::string letters = "abcdefghijklmnopqrstuvwxyz";
		static const std::string punctuation = ".,;:!?'\"- ";
		static const std::string all = letters + punctuation;
		
		extern const std::map<char, double> monograms;
		extern const std::map<std::string, double> bigrams;
		extern const std::map<std::string, double> trigrams;
		extern const std::map<std::string, double> quadgrams;
		extern const std::map<std::string, double> quintgrams;
		extern const std::map<std::string, double> words;
		extern const std::vector<std::string> keywords;
		
	} // alphabets
	
	std::map<char, double> charactersUsage(const std::string& alphabet, bool alphabetOnly = true);
	
} // statistics
} // decoders

#endif /* statistics_hpp */
