//
//  main.cpp
//  Decryptor
//
//  Created by iKing on 14.09.17.
//  Copyright © 2017 iKing. All rights reserved.
//

#include <iostream>
#include <iomanip>
#include <chrono>
#include <set>
#include <sstream>
#include "decoders.hpp"
#include "samples.hpp"
#include "helpers.hpp"
#include "statistics.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
	
	const unsigned int seed = (unsigned int) time(nullptr);
	cout << "Seed: " << seed << endl;
	srand(seed);
	
	std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
	
//	cout << decoders::decodeSingleByteXor(samples.singleByteXor) << endl;

//	cout << decoders::decodeRepeatingKeyXor(samples.repeatingKeyXor) << endl;

//	std::string string = samples.simpleSubstitution;
//	std::transform(string.begin(), string.end(), string.begin(), ::tolower);
//	std::cout << decoders::decodeSimpleSubstitution(string) << std::endl;
	
//	std::string string = samples.poylayphabeticSubstitution;
	
	std::string string = samples.poylayphabeticSubstitutionWithTypos;
	
	std::transform(string.begin(), string.end(), string.begin(), ::tolower);
	cout << decoders::decodePoylayphabeticSubstitution(string) << endl;
	
	cout << "-----------------------------------\n";
	cout << "Total execution time: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - startTime).count() << "ms" << endl;
	
	
	return 0;
}

